package dev.dextra.newsapp.feature.news

import dev.dextra.newsapp.TestConstants
import dev.dextra.newsapp.api.model.ArticlesResponse
import dev.dextra.newsapp.base.BaseTest
import dev.dextra.newsapp.base.NetworkState
import dev.dextra.newsapp.base.TestSuite
import dev.dextra.newsapp.feature.sources.SourcesViewModel
import dev.dextra.newsapp.utils.JsonUtils
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.koin.test.get


class NewsViewModelTest : BaseTest() {

    val emptyResponse = ArticlesResponse(ArrayList(), "ok", 20)

    lateinit var viewModel: NewsViewModel

    @Before
    fun setupTest() {
        viewModel = TestSuite.get()
    }

    @Test
    fun testGetArticles() {
        val sourcesViewModel: SourcesViewModel = TestSuite.get()
        sourcesViewModel.loadSources()

        val source = sourcesViewModel.sources.value!![0]
        viewModel.configureSource(source)
        viewModel.loadNews()

        assert(viewModel.articles.value?.size != 0)
        assertEquals(NetworkState.SUCCESS, viewModel.networkState.value)

        viewModel.onCleared()

        assert(viewModel.disposables.isEmpty())
    }

    @Test
    fun testEmptyArticles() {
        TestSuite.mock(TestConstants.newsURL).body(JsonUtils.toJson(emptyResponse)).apply()

        viewModel.loadNews()

        assert(viewModel.articles.value?.size == 0)
        assertEquals(NetworkState.EMPTY, viewModel.networkState.value)
    }

    @Test
    fun testErrorArticles() {
        TestSuite.mock(TestConstants.newsURL).throwConnectionError().apply()

        viewModel.loadNews()

        assert(viewModel.articles.value == null)
        assertEquals(NetworkState.ERROR, viewModel.networkState.value)
    }
}