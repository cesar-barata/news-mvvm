package dev.dextra.newsapp

val API_KEY_HEADER_NAME = "X-Api-Key"
val API_KEY = "7a1665ae793a4a8bb1afcc9db326f032"
val BASE_URL = "https://newsapi.org"
val ICON_LOCATOR_URL = "https://icon-locator.herokuapp.com/icon?url=%s&size=30..60..150"
val IMAGE_LOCATOR_URL = "https://icon-locator.herokuapp.com/icon?url=%s&size=100..300..500"