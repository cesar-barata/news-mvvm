package dev.dextra.newsapp.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    val disposables = mutableListOf<Disposable>()

    fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun clearDisposables() {
        disposables.forEach {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
        disposables.clear()
    }

    public override fun onCleared() {
        clearDisposables()
        super.onCleared()
    }

}