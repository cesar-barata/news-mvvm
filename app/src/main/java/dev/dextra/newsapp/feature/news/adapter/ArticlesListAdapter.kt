package dev.dextra.newsapp.feature.news.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.dextra.newsapp.R
import dev.dextra.newsapp.api.model.Article
import kotlinx.android.synthetic.main.item_article.view.*

class ArticlesListAdapter(val listener: ArticlesListAdapterItemListener) :
    RecyclerView.Adapter<ArticlesListAdapter.ArticlesListAdapterViewHolder>()  {

    private val dataset: ArrayList<Article> = ArrayList()

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: ArticlesListAdapterViewHolder, position: Int) {
        val article = dataset[position]

        holder.view.setOnClickListener { listener.onClick(article) }
        holder.view.article_name.text = article.title
        holder.view.article_description.text = article.description
        holder.view.article_date.text = article.publishedAt
        holder.view.article_author.text = article.author
    }

    fun add(articles: List<Article>) {
        dataset.addAll(articles)
    }

    fun clear() {
        dataset.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlesListAdapterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_article, parent, false)
        return ArticlesListAdapterViewHolder(view)
    }

    class ArticlesListAdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    interface ArticlesListAdapterItemListener {
        fun onClick(article: Article)
    }

}