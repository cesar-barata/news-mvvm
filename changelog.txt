2019-09-19:
    - Correção de NullPointerException devido ao uso do operator !!
    - Substituição de chave expirada do serviço NewsAPI

2019-09-23:
    - Refatoração da "feature" de notícias para seguir o padrão MVVM.
        - Criação do NewsViewModel
        - Refatoração da NewsActivity para acomodar o NewsViewModel e
        alteração da sua classe base para BaseListActivity para 
        reaproveitamento do código usado no gerenciamento de telas de
        carregamento, mensagens de erro e orientação da tela.
    - Correção das transições dos estados de rede.
    - Adição de testes unitários para a "feature" de notícias